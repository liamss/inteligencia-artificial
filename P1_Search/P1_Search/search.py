# search.py
# ---------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


"""
In search.py, you will implement generic search algorithms which are called by
Pacman agents (in searchAgents.py).
"""

import util

"""problema 1"""
from game import Directions
from util import Stack

"""problema 2"""
from game import Directions
from util import Queue

"""problema 3 y 4"""
from util import PriorityQueue

class SearchProblem:
    """
    This class outlines the structure of a search problem, but doesn't implement
    any of the methods (in object-oriented terminology: an abstract class).

    You do not need to change anything in this class, ever.
    """

    def getStartState(self):
        """
        Returns the start state for the search problem.
        """
        util.raiseNotDefined()

    def isGoalState(self, state):
        """
          state: Search state

        Returns True if and only if the state is a valid goal state.
        """
        util.raiseNotDefined()

    def getSuccessors(self, state):
        """
          state: Search state

        For a given state, this should return a list of triples, (successor,
        action, stepCost), where 'successor' is a successor to the current
        state, 'action' is the action required to get there, and 'stepCost' is
        the incremental cost of expanding to that successor.
        """
        util.raiseNotDefined()

    def getCostOfActions(self, actions):
        """
         actions: A list of actions to take

        This method returns the total cost of a particular sequence of actions.
        The sequence must be composed of legal moves.
        """
        util.raiseNotDefined()


def tinyMazeSearch(problem):
    """
    Returns a sequence of moves that solves tinyMaze.  For any other maze, the
    sequence of moves will be incorrect, so only use this for tinyMaze.
    """
    from game import Directions
    s = Directions.SOUTH
    w = Directions.WEST
    return  [s, s, w, s, w, w, s, w]

def depthFirstSearch(problem: SearchProblem):
    """
    Search the deepest nodes in the search tree first.

    Your search algorithm needs to return a list of actions that reaches the
    goal. Make sure to implement a graph search algorithm.

    To get started, you might want to try some of these simple commands to
    understand the search problem that is being passed in:

    print("Start:", problem.getStartState())
    print("Is the start a goal?", problem.isGoalState(problem.getStartState()))
    print("Start's successors:", problem.getSuccessors(problem.getStartState()))
    """

    """
    Búsqueda en profundidad en un grafo para encontrar un punto de comida fijo.

    Args:
    - problem: Un objeto Problem que describe el problema de búsqueda.

    Returns:
    - Una lista de acciones que llevan al agente desde la start_state a un estado objetivo.
    """
    start_state = problem.getStartState()
    frontier = Stack()
    frontier.push((start_state, []))  # (estado_actual, acciones_hasta_este_estado)
    explored = set()

    while not frontier.isEmpty():
        current_state, actions = frontier.pop()

        if problem.isGoalState(current_state):
            return actions

        if current_state not in explored:
            explored.add(current_state)
            successors = problem.getSuccessors(current_state)
            for successor_state, action, _ in successors:
                if successor_state not in explored:
                    frontier.push((successor_state, actions + [action]))

    return []  # Si no se encuentra una solución


    """util.raiseNotDefined() //esto cuando tengamos el codigo quitarlo"""

def breadthFirstSearch(problem: SearchProblem):
    """
    Búsqueda en anchura en un grafo para encontrar un punto de comida fijo.

    Args:
    - problem: Un objeto Problem que describe el problema de búsqueda.

    Returns:
    - Una lista de acciones que llevan al agente desde la start_state a un estado objetivo.
    """
    start_state = problem.getStartState()
    frontier = Queue()
    frontier.push((start_state, []))  # (estado_actual, acciones_hasta_este_estado)
    explored = set()

    while not frontier.isEmpty():
        current_state, actions = frontier.pop()

        if problem.isGoalState(current_state):
            return actions

        if current_state not in explored:
            explored.add(current_state)
            successors = problem.getSuccessors(current_state)
            for successor_state, action, _ in successors:
                if successor_state not in explored:
                    frontier.push((successor_state, actions + [action]))

    return []  # Si no se encuentra una solución

# Resto del código...


def uniformCostSearch(problem: SearchProblem):
    """
    Search the node of least total cost first.

    Args:
    - problem: A SearchProblem object describing the problem to be solved.

    Returns:
    - A list of actions that reaches the goal.
    """
    start_state = problem.getStartState()
    frontier = PriorityQueue()  # Use PriorityQueue for UCS
    frontier.push((start_state, [], 0), 0)  # (current_state, actions, total_cost)
    explored = set()

    while not frontier.isEmpty():
        current_state, actions, total_cost = frontier.pop()

        if problem.isGoalState(current_state):
            return actions

        if current_state not in explored:
            explored.add(current_state)
            successors = problem.getSuccessors(current_state)
            for successor_state, action, step_cost in successors:
                if successor_state not in explored:
                    new_actions = actions + [action]
                    new_total_cost = total_cost + step_cost
                    frontier.push((successor_state, new_actions, new_total_cost), new_total_cost)

    return []  # If no solution is found


def nullHeuristic(state, problem=None):
    """
    A heuristic function estimates the cost from the current state to the nearest
    goal in the provided SearchProblem.  This heuristic is trivial.
    """
    return 0

def aStarSearch(problem: SearchProblem, heuristic=nullHeuristic):
    """
    Search the node that has the lowest combined cost and heuristic first.

    Args:
    - problem: A SearchProblem object describing the problem to be solved.
    - heuristic: A heuristic function that estimates the cost from the current state to the goal.

    Returns:
    - A list of actions that reaches the goal.
    """
    start_state = problem.getStartState()
    frontier = PriorityQueue()  # Use PriorityQueue for A*
    frontier.push((start_state, [], 0), heuristic(start_state, problem))
    explored = set()

    while not frontier.isEmpty():
        current_state, actions, total_cost = frontier.pop()

        if problem.isGoalState(current_state):
            return actions

        if current_state not in explored:
            explored.add(current_state)
            successors = problem.getSuccessors(current_state)
            for successor_state, action, step_cost in successors:
                if successor_state not in explored:
                    new_actions = actions + [action]
                    new_total_cost = total_cost + step_cost
                    priority = new_total_cost + heuristic(successor_state, problem)
                    frontier.push((successor_state, new_actions, new_total_cost), priority)

    return []  # If no solution is found


# Abbreviations
bfs = breadthFirstSearch
dfs = depthFirstSearch
astar = aStarSearch
ucs = uniformCostSearch
